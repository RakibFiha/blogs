---
layout: post
title: Example Express App
date: 2017-08-21 13:32:20 +0300
description: You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes. # Add post description (optional)
img: express.png # Add image post (optional)
tags: [Blog, Express, NodeJS, form, addingform, post, request]
author: # Add name author (optional)
---
# Express Routing Example Express

1. Creating a brand new express app from scratch
2. Create a package.json using `npm init` and add express as a dependency
3. In the main app.js file, adding 3 different routes:

Visiting "/" should print "Hi, there, welcome to this example"

===========================================================

Visiting "speak/pig should print "The pig says 'oink'"
Visiting "speak/cow should print "The pig says 'moo'"
Visiting "speak/dog should print "The pig says 'woof woof'"

===========================================================

Visiting "repeat/hello/3 should print "hello hello hello"
Visiting "repeat/hello/5 should print "hello hello hello hello hello"
Visiting "repeat/blah/3 should print "blah blah blah"

If a user visits any other route, print:
"Sorry, page not found... Fook is gone maybe, init, bruv!!"

```
var express = require("express");
var app = express();

app.get("/", function(req,res){
  res.send("Hi, there, welcome to this example!!");
  console.log("Someone has gone to the main page init!!");
})

app.get("/speak/:creature", function(req,res){
//   res.send("Creature says what?");
  var creatures = req.params.creature;

// logic
  // if creature is this print this
  if (creatures == "dog"){
    res.send("The "+ creatures +" says 'Woof Woof!'");
    console.log("The "+ creatures +" says 'Woof Woof!'");
  }
  else if (creatures == "cow"){
    res.send("The "+ creatures +" says 'Moo!'");
    console.log("The "+ creatures +" says 'Moo!'");
  }
  else if (creatures == "pig"){
    res.send("The "+ creatures +" says 'Oink!'");
    console.log("The "+ creatures +" says 'Oink!'");
  }
  else if (creatures == creatures){
    res.send("The "+ creatures +" says 'you!'");
    console.log("The "+ creatures +" says 'you!'");
  }
})

app.get("*", function(req, res){
  res.send("Sorry, page not found... Fook is gone maybe, init, bruv!!");
  console.log("Someone has gone to limbo init!!");
})

app.listen(3000, function(){
  console.log("server has started on port 3000");
})
```
the following is another way of doing the same shit

```
var express = require("express");
var app = express();

app.get("/", function(req,res){
  res.send("Hi, there, welcome to this example!!");
  console.log("Someone has gone to the main page init!!");
})

app.get("/speak/:creature", function(req,res){
//   res.send("Creature says what?");
	var creature = req.params.creature;
	var sound = "";
	if(creature === "pig"){
		sound = "oink";
	} else if(creature === "cow"){
		sound ="moo";
	} else
		sound = "I dont like you";
 	res.send("The "+ creature + " says " + sound);
	console.log("The "+ creature + " says " + sound);
});

app.get("*", function(req, res){
  res.send("Sorry, page not found... Fook is gone maybe, init, bruv!!");
  console.log("Someone has gone to limbo init!!");
})

app.listen(3000, function(){
  console.log("server has started on port 3000");
})

```
the following even better, using the power of template literals \` \`

```
var express = require("express");
var app = express();

app.get("/", function(req, res) {
	res.send("Hi, there, welcome to this example!!");
	console.log("Someone has gone to the main page init!!");
})

app.get("/speak/:creature", function(req, res) {
	//   res.send("Creature says what?");
	var sounds = {
		pig: "Oink",
		cow: "Moo",
		cat: "mew",
		fish: "tada",
		dog: "I dont like it"
	}

	var creature = req.params.creature.toLowerCase();
	var sound = sounds[creature];
	// use template literals it will make life easy
	// template literals have super powers
	res.send(`The  ${creature}  says '${sound}'`);
});

app.get("/repeat/:message/:times", function(req, res) {
	var message = req.params.message;
	var times = Number(req.params.times);
	var result = "";

	// using template literals here again
	// template literals is the best

	for (var i = 0; i < times; i++) {
		result += `${message} `;
	}
	res.send(`${result}`);
});

app.get("*", function(req, res) {
	res.send("Sorry, page not found... Fook is gone maybe, init, bruv!!");
	console.log("Someone has gone to limbo init!!");
})
```
More Express, here is how to add post request and add a form in express. This is simple example. Before proceeding make sure to install bodyParser using `npm install body-parser --save` for it to work. Body parser package allows the post request to happen.

then make a new file in views directory as coins.ejs

here is the coins.ejs file
```
<h1>
The Diversification list of portfolio
</h1>

<% coins.forEach(function(coins){ %>
	<li><%= coins %></li>
<% }); %>

<form action="/addcoin" method="POST">
	<input type="text" name="newcoins" placeholder="name">
	<button> New coin is added!</button>
</form>
```

here is the express code example.
```
var express = require("express");
var app = express();
var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended:true}));

// this will make sure that you dont have
// to put ejs extension all the time
app.set("view engine", "ejs");

var coins = ["Bitcoin","Ethereum", "Dash", "Litecoin", "Vertcoin"];


app.get("/", function(req, res){
	res.render("home");
});

app.post("/addcoin", function(req, res){
	var newCoin = req.body.newcoins;
	coins.push(newCoin);
	res.redirect("/mycoins");
// 	res.send("You have reached the post route!!");
});

app.get("/mycoins", function(req,res){
	res.render("coins", {coins: coins});
});

app.get("*", function(req, res){
  res.send("Star is undefined fook");
  console.log("Someone is undefined fook!");
});

app.listen(3000, function(){
	console.log(`Server started at
	https://node-rakibfiha2726789.codeanyapp.com
	https://node-rakibfiha2726789.codeanyapp.com/mycoins`);
});

```
