---
layout: post
title: Express App
date: 2018-10-24 13:32:20 +0300
description: You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes. # Add post description (optional)
img: express.png # Add image post (optional)
tags: [Blog, express, simple format]
author: # Add name author (optional)
---
# Express Routing

For express we need this following steps, which is about 100 lines.

1. Declare variables
2. App config
3. Mongoose/ model config
4. RESTful routes
5. New route
6. Create route
7. Show route
8. Edit route
9. Put or update route
10. Delete route

# 1. Declare variables:

In express we may normally see these following packages to be used. We can specify them by this:

```
const express = require('express'),
      expressSanitizer = require('express-sanitizer'),
      methodOverride = require('method-override'),
      mongoose = require('mongoose'),
      bodyParser = require('body-parser'),
      app = express();
```

It is wise to see the packages installed for the project in the package.json file.

# 2. App config

Sample app config:

```
// APP config
mongoose.connect("mongodb://localhost/restful_blog_app", {useNewUrlParser: true});
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({extended:true}));
app.use(expressSanitizer());
app.use(express.static(__dirname + '/public/'));
app.use(methodOverride("_method"));

```

# 3. Mongoose / Model Config

Sample code:

```
// Mongoose / Model Config
const blogSchema = new mongoose.Schema({
  title: String,
  image: String,
  body: String,
  created: {type: Date, default: Date.now}
});

const Blog = mongoose.model("Blog", blogSchema);
```

# 4. RESTful routes

Sample code of RESTful routes

```
// RESTFUL Routes

app.get("/", function(req,res){
  res.redirect("/blogs");
});

app.get("/blogs", function(req,res){
  Blog.find({}, function(err, blogs){
    if(err){
      console.log("Error!");
    } else {
      res.render("index", {blogs: blogs});
    }
  });
});
```

# 5. New Route

In order to create a new blog, new route is used to get requests from the server
Sample code:

```
// NEW Route
app.get("/blogs/new", function(req, res){
  res.render("new");
});
```

# 6. Create Route

Using the create route a new blog can be added.
Sample code:

```
// CREATE ROUTE
app.post("/blogs", function(req,res){
  // Create blog
  console.log(req.body);
  req.body.blog.body = req.sanitize(req.body.blog.body);
  console.log("=======");
  console.log(req.body);
  Blog.create(req.body.blog, function(err, newBlog){
    if(err){
      res.render("new");
    } else {
      // then, redirect to the index
      res.redirect("/blogs");
    }
  });
});
```

# 7. Show Route

Show route is used to see the existing blog posts.
Sample code:

```
// Show ROUTE
app.get("/blogs/:id", function(req, res){
  Blog.findById(req.params.id, function(err, foundBlog){
    if(err){
      res.redirect("/blogs");
    } else {
      res.render("show", {blog: foundBlog});
    }
  });
});
```

# 8. Edit Route

To make changes to any blog post, edit route is used.
Sample code:

```
// Edit Route
app.get("/blogs/:id/edit", function(req, res){
  Blog.findById(req.params.id, function(err, foundBlog){
    if(err){
      res.redirect("/blogs");
    } else {
      res.render("edit", {blog: foundBlog});
    }
  });
});

```

# 9. Put or Update route

After edit, the put or update route send requests to the server to make changes.
Sample code:

```
// Put Update ROUTE
app.put("/blogs/:id", function(req, res){
  req.body.blog.body = req.sanitize(req.body.blog.body);
  Blog.findByIdAndUpdate(req.params.id, req.body.blog, function(err, updatedBlog){
    if(err){
      res.redirect("/blogs");
    } else {
      res.redirect("/blogs/" + req.params.id);
    }
  });
});
```

# 10. Delete Route

Delete route is used to delete an existing blog post.
Sample code:

```
// Delete Route
app.delete("/blogs/:id", function(req, res){
  // destroy
  Blog.findByIdAndRemove(req.params.id, function(err){
    if(err){
      res.redirect("/blogs");
    } else {
      res.redirect("/blogs");
    }
  });
});
```
# Ending of the app.js

Do not forget to include listen to request like the following:
```
app.listen(3000, function(){
	console.log("Server is running!")
})
```
