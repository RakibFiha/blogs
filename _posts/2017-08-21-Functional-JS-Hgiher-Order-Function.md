---
layout: post
title: Web API- Web Programming Application
date: 2017-08-21 13:32:20 +0300
description: You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes.
img: express.png # Add image post (optional)
fig-caption: # Add figcaption (optional)
tags: [Blog, functional-programming, js, higher-order-functions]
author: # Add name author (optional)
---

# Higher Order functions

In JS functions are values, for example:

```
var tripple = function(x){
  return x * 3
}

var shadna = tripple
shadna(30)
// will return
// 90
```

Like variables and other stuff, functions can be pass into variables, or also can be passed into other functions.
Also known as higher order functions.

Higher order functions are good for compositions. For example:

Using the filter functions you can filter. Read more about filter(!https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter)

```
var animals =[
  {name:"Fluffy", species:"cat"},
  {name:"Truffy", species:"cat"},
  {name:"Enori", species:"faggot"},
  {name:"Shadna", species:"maggot"},
  {name:"Pdna", species:"dog"},
  {name:"Jaqu", species:"planet"}
]

// // filter using for loop
var dogs = []
for (var i = 0; i < animals.length; i++) {
  if(animals[i].species === "dog")
    dogs.push(animals[i]);
    console.log(dogs);
}

// // filter using functional programming filter
var dogs = animals.filter(function(animal){
  return animal.species === "dog";
  console.log(dogs);
})

// // map using for loop
var names = []
for (var i = 0; i < animals.length; i++) {
  names.push(animals[i].name)
  console.log(names);
}

// // map using functional programming map
var names = animals.map(function(animal){
  return animal.name;
  console.log(names);
});

// // map using ES6 syntax
var names = animals.map((animal) => {return animal.name})
// // or more shortly
var names = animals.map((animal) => animal.name)

```
