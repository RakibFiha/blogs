---
layout: post
title: Zencash Commands for secure nodes
date: 2017-09-12 00:00:00 +0300
description: You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes. # Add post description (optional)
img: zencash.jpg # Add image post (optional)
fig-caption: # Add figcaption (optional)
tags: [Zencash, Secure nodes, pm2, node, tracker] # add tag
---


![Macbook]({{site.baseurl}}/assets/img/zencash.jpg)

Update and upgrade first:

```
sudo apt-get update && time sudo apt-get dist-upgrade
```

Some Zen commands

To check block propagation

```
zen-cli getinfo
```

to check tls certificate if true

```
zen-cli getnetworkinfo
```

to import private keys.
```
zen-cli importprivkey <keys>
```

to dump private keys from the node

```
zen-cli dumpprivkey <t-addr>
```

to generate new t-address
```
 zen-cli getnewaddress > /dev/null && zen-cli listaddresses | jq -r '.[1]'
```
to see list of addresses in the node
```
zen-cli listaddresses
```
to change the notifying email address in zencash, you have to re-run the setup by first going to the directory where it is installed, and then run the node.
and then need to restart your tracker to update the registered email address

```
node setup.js
```
[Here is the direct link to the website for reference](https://zencash.atlassian.net/wiki/spaces/ZEN/pages/149225473/Upgrading+Tracker+Software+Legacy+-+PM2+Monit)

For new nodes, transferring funds to new z address like this:

```
zen-cli z_sendmany $(zen-cli listaddresses | jq -r '.[0]') '[{"address": "'$(zen-cli z_getnewaddress)'", "amount": 0.0249},{"address": "'$(zen-cli z_getnewaddress)'", "amount": 0.0249}]'
```

before executing this command check if this `zen-cli listaddresses | jq -r '.[0]'` gives you the right output. If imported later its likely that the index will be 0, but if the addresses used was already in the node then it can be 1. But its good to double check.

after sending funds to z-addr you can check the balance by typing the following below and this will update the balance every 30 seconds.

```
watch -n 30 zen-cli z_gettotalbalance
```
after typing `zen-cli getnetworkinfo` if tls certificate is true that means zen.conf is configured correctly.

Using zen-cli to send Zen to addresses: [Here is the direct link to the website for reference](https://zencash.atlassian.net/wiki/spaces/ZEN/pages/7537181/System+Maintenance)

```
zen-cli listaddresses
```

For z_addresses

```
zen-cli z_listaddresses
```

Check balances: For all t_addresses

```
zen-cli listaddressgroupings
```

For total balance (t_addresses [transparent] and z_addresses [private])

```
zen-cli z_gettotalbalance
```

For individual z_addresses (replace z_address with the value)

```
zen-cli z_getbalance z_address
```

Exporting / dumping private keys

For t_addresses (replace t_address with the value)

```
zen-cli dumpprivkey t_address
```

For z_addresses (replace z_address with the value)

```
zen-cli z_exportkey z_address
```

Checking block height

```
zen-cli getinfo | grep blocks
```

Sending transactions via the zend daemon

Replace;

fromAddress with a t_address, or z_address
toAddress with a t_address, or z_address
amt with an amount (numeric)

```
zen-cli z_sendmany 'fromAddress' '[{"address": "toAddress", "amount": amt}]'
```

Start, stop and restart processes
How to start, stop, restart and check the status of zend

For installations using systemd (assumes the unit file is named 'zend.service')

```
sudo systemctl start zend
sudo systemctl stop zend
sudo systemctl restart zend
sudo systemctl status zend
```

For installations using monit

```
sudo monit start zend
sudo monit stop zend
sudo monit restart zend
sudo monit status
```

How to start, stop, restart and check the status of the node tracker
For installations using systemd (assumes the unit file is named 'zentracker.service')

```
sudo systemctl start zentracker
sudo systemctl stop zentracker
sudo systemctl restart zentracker
sudo systemctl status zentracker
```

For installations using pm2 (assumes the tracker is running as the only or first process, numbered 0)

```
pm2 stop 0
pm2 stop 0
pm2 restart 0
pm2 status 0
```

Checking logs
How to follow the zend log
Follow the zend output log with the command below, quit by pressing CTRL+c

```
tail -f ~/.zen/debug.log
```

How to follow the node tracker log
For installations using systemd (assumes the unit file is named 'zentracker.service') quit by pressing CTRL+c

```
sudo journalctl -fu zentracker
```

For installations using pm2 (assumes the tracker is running as the only or first process, numbered 0)

```
pm2 logs
Or to tail pm2 logs directly
```
```
tail -f ~/.pm2/logs/secnodetracker-out-0.log
```

How to obtain zend logs (if needed to raise a help ticket). This command will create a file named 'zendlog.txt' in the user's home directory

```
cp ~/.zen/debug.log ~/zendlog.txt
```

How to obtain node tracker logs (if needed to raise a help ticket). Either command will create a file named 'trackerlog.txt' in the user's home directory
For installations using systemd (assumes the unit file is named 'zentracker.service')

sudo journalctl --no-pager -u zentracker > ~/trackerlog.txt
For installations using pm2 (assumes the tracker is running as the only or first process, numbered 0)

```
cp ~/.pm2/logs/secnodetracker-out-0.log > ~/trackerlog.txt
```
