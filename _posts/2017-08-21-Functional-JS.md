---
layout: post
title: Web API- Web Programming Application
date: 2017-08-21 13:32:20 +0300
description: You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes. # Add post description (optional)
img: express.png # Add image post (optional)
tags: [Blog, functional, js, database, tree structure]
author: # Add name author (optional)
---
# Doing loop using functional JS

* Here some thoughts on functional programming

This block of code will do a loop and will count down from `1 to 10`. Notice that `console.log(num)` is called before `countDown(num-1)`, if done otherwise the number will be printed from `10 to 1`

```
let countDown = (num)=>{
  if (num === 0) return;
  countDown(num-1);
  console.log(num);
}
countDown(10)
```

Print number opposite from `10 to 1`

```
let countDown = (num)=>{
  if (num === 0) return;
  console.log(num);
  countDown(num-1);
}
countDown(10)
```

Example of splitting a db into tree structure.

```
let categories = [
  {id: 'animals', 'parent': null},
  {id: 'mammals', 'parent': 'animals'},
  {id: 'cats', 'parent': 'mammals'},
  {id: 'dogs', 'parent': 'mammals'},
  {id: 'chi', 'parent': 'dogs'},
  {id: 'lab', 'parent': 'dogs'},
  {id: 'persian', 'parent': 'cats'},
  {id: 'siamese', 'parent': 'cats'}
]

// output should look like this:
/*
{
  animals: {
    mammals:{
      dogs:{
        chi: null
        lab: null
      },
      cats:{
        persian: null
        siamese: null
      }
    }
  }
}
*/

let makeTree = (categories, parent) => {
  let node = {}
  categories
    .filter(c => c.parent === parent)
    .forEach(c =>
      node[c.id] = makeTree(categories, c.id))
  return node
}

console.log(
  JSON.stringify(
    makeTree(categories, null)
  , null, 2)
)
```
