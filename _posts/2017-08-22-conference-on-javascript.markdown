---
layout: post
title: More Express JS with NodeJS
date: 2017-09-10 00:00:00 +0300
description: You’ll find this post in your `_posts` directory. Go ahead and edit it and re-build the site to see your changes. # Add post description (optional)
img: js-1.png # Add image post (optional)
tags: [Js, NodeJS, Express, EJS] # add tag
---
# Rendering HTML and Templates

* Use res.render() to render HTML(from an EJS file)
* Explain what EJS is and why we use it
* Pass variables to EJS templates

First create a new directory and do

```
npm init
```
then install express by
```
npm install express --save
```

Example of app.js file:

```
var express = require("express")
var app = express();

app.get("/", function(req, res){
  // as you can see it gets tiring to write web pages like this as following
  // res.send("Welcome to the page")
  // we can replace it like this:
	res.send("<h1>Welcome to the homepage!</h1> <h2>blah blah blah</h2>");
  // but still it is a lot of hard work
  // what we can do now is that, we can refer to a different html file from a
  // different place, that would make life a lot easier like this:
  res.render("dogs.html");
  // we call it rendering. but adding html is problematic also due to web static-ness.
  // we are looking for dynamic html files that we call templates
  // so we introduce EJS or Embedded Javascript
});

app.listen(3000, function(){
	console.log("server is listening!!")
});
```
# EJS - Embedded Javascript

install EJS by

```
npm install ejs --save
```
then do this like before, __IMportant__ remember that express will look for any files in /views directory. so you must save the ejs file (here, home.ejs file for example) in /views directory

```
var express = require("express")
var app = express();
// many people write it like this:
// var app = require("express")();
// but in most cases we write it like that as we mentioned before

app.get("/", function(req, res){
	res.render("home.ejs");

});

app.listen(3000, function(){
	console.log("server is listening!!")
});
```

In ejs we can write regular JS objects inside the <%= here %>

for example, if want our stuff to be turned into uppercase we can simply do
<%= here.toUpperCase() %>

```
var express = require("express")
var app = express();

app.get("/", function(req, res){
	res.render("home.ejs");
});

app.get("/shanda/:thing", function(req, res){
	var ting = req.params.thing;
	res.render("ting.ejs", {tingShadna: ting});
});

app.listen(3000, function(){
	console.log("server is listening!!")
});
```

Make sure to create the ting.ejs file and make sure to define tingShadna inside it like this for example:

```
<h1> Ting is shadna, and shadna is ting, but what did you want: <%=tingShadna.toUpperCase()%> </h1>
```
after that you can make something more simple like this:
```
<h1>
 You fell in love with: <%= tingShadna.toUpperCase() %>
</h1>

<% if(tingShadna.toLowerCase() === "shanda"){ %>
<p>
You have chosen good! Shanda is inside the pune
</p>
<% } else { %>
<p>
  Bad choice! Shanda should have been chosen
</p> <%
} %>

<h2>
 P.S this is renderTing.ejs file!
</h2>
```
Can create an another ejs file posts.ejs with the following:

```
<h1> The Posts Page </h1>

<% for(var i = 0; i < posts.length; i++){ %>
	<li>
			<%= posts[i].title %> - <strong><%= posts[i].author %></strong>
	</li>
<% } %>

<% posts.forEach(function(posts){ %>
	<li>
			<%= posts.title %> - <strong><%= posts.author %></strong>
	</li>
<% }) %>
```
This would be the following app.js file for the above mentioned posts. and make sure to save the ejs files in views directory.

```
var express = require("express");
var app = express();

app.get("/", function(req, res){
	res.render("home.ejs");
});

app.get("/shanda/:thing", function(req, res){
	var ting = req.params.thing;
	res.render("renderTing.ejs", {tingShadna: ting});
});

app.get("/posts", function(req,res){
	var posts = [
		{title: "shanda post 1", author: "Susy shanda 1"},
		{title: "shanda post 2", author: "Susy shand 2"},
		{title: "Shanda post 3", author: "Susy shan 3"}
	];

	res.render("posts.ejs", {posts: posts});
})
app.listen(3000, function(){
	console.log("server is listening!!")
});

```

you will see a output like this:

```
The Posts Page
shanda post 1 - Susy shanda 1
shanda post 2 - Susy shand 2
Shanda post 3 - Susy shan 3
shanda post 1 - Susy shanda 1
shanda post 2 - Susy shand 2
Shanda post 3 - Susy shan 3
```
